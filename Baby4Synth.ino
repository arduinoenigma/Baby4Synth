// Arduino synth library Output mode example.

// The synth can output audio in three modes on two different pins:

// OUTA = Arduino PIN 11
// OUTB = Arduino PIN 3

// or

// Differntial on PINA,PINB

//Hardware connection(s):

//                           +10µF
//PIN 11 (OUTA)---[ 1k ]--+---||--->> Audio out
//                        |
//                       === 10nF
//                        |
//                       GND

//                           +10µF
//PIN 3 (OUTB) ---[ 1k ]--+---||--->> Audio out
//                        |
//                       === 10nF
//                        |
//                       GND

// Differential mode gives twice the voltage swing and may be better for driving piezos or speakers directly.
// Here are some examples:

// PIN 11 (OUTA) ------- Speaker/Piezo +

// PIN 3 (OUTB)  ------- Speaker/Piezo -

// Or better with a low pass filter

// PIN 11 (OUTA) ----+
//                   |
//                    )
//                    ) 10mH inductor
//                    )
//                   |
//                   +---- Speaker/Piezo +
//                   |
//                  === 1µF
//                   |
//PIN 3 (OUTB)  -----+---- Speaker/Piezo -


#include "synth.h"
synth edgar;

byte Voices[][5] = {
  {0, 60, 0, 90, 64},
  {0, 65, 0, 90, 64},
  {0, 70, 0, 90, 64},
  {0, 75, 0, 90, 64}
};

byte parserstate = 0;
byte param = 0;
byte value = 0;
byte currentvoice = 0;

unsigned long nextentrytime = 0;

void setup() {

  //You select the output mode when starting the synth. You may re-start any time in any mode by calling apropriate begin()
  //Uncomment desired:

  Serial.begin(19200);
  Serial.println("The Synth...");

  edgar.begin(DIFF);        //Default OUTA
  //edgar.begin(CHA);     //CHA (same as default)
  //edgar.begin(CHB);     //CHB
  //edgar.begin(DIFF);    //Differential

  //  Set up the voices
  //  voice[0-3],wave[0-6],pitch[0-127],envelope[0-4],length[0-127],mod[0-127:64=no mod]

  //  setWave(voice,wave);
  //  setPitch(voice,pitch);
  //  setEnvelope(voice,env);
  //  setLength(voice,length);
  //  setMod(voice,mod);

  edgar.setupVoice(0, Voices[0][0], Voices[0][1], Voices[0][2], Voices[0][3], Voices[0][4]);
  edgar.setupVoice(1, Voices[1][0], Voices[1][1], Voices[1][2], Voices[1][3], Voices[1][4]);
  edgar.setupVoice(2, Voices[2][0], Voices[2][1], Voices[2][2], Voices[2][3], Voices[2][4]);
  edgar.setupVoice(3, Voices[3][0], Voices[3][1], Voices[3][2], Voices[3][3], Voices[3][4]);

  nextentrytime = micros() + 10000;
}

byte voiceplay = 0;
byte voicetime = 100;

void loop()
{
  byte inByte = 0;

  if (Serial.available() > 0) {
    inByte = Serial.read();

    switch (parserstate)
    {
      case 0:
        param = 0;
        value = 0;
        if (inByte == 'V')
        {
          param = 1;
          parserstate = 1;
        }
        if (inByte == 'W')
        {
          param = 2;
          parserstate = 1;
        }
        if (inByte == 'P')
        {
          param = 3;
          parserstate = 1;
        }
        if (inByte == 'E')
        {
          param = 4;
          parserstate = 1;
        }
        if (inByte == 'L')
        {
          param = 5;
          parserstate = 1;
        }
        if (inByte == 'M')
        {
          param = 6;
          parserstate = 1;
        }
        if (inByte == 'T')
        {
          param = 7;
          parserstate = 1;
        }

        break;

      case 1:
        if ((inByte > 47) && (inByte < 58)) {
          value *= 10;
          value += inByte - '0';
        }
        if (inByte == ';')
        {
          Serial.print("P:");
          Serial.print(param);
          Serial.print("V:");
          Serial.println(value);
          parserstate = 0;

          if ((param == 1) && (value < 4))
          {
            currentvoice = value;
          }

          if ((param == 2) && (value < 7))
          {
            Voices[currentvoice][0] = value;
          }

          if ((param == 3) && (value < 128))
          {
            Voices[currentvoice][1] = value;
          }

          if ((param == 4) && (value < 5))
          {
            Voices[currentvoice][2] = value;
          }

          if ((param == 5) && (value < 128))
          {
            Voices[currentvoice][3] = value;
          }

          if ((param == 6) && (value < 128))
          {
            Voices[currentvoice][4] = value;
          }

          if ((param == 7))
          {
            voicetime = value;
          }

          //edgar.setupVoice(0,TRIANGLE,60,ENVELOPE1,127,64);

          //  Set up the voices
          //  voice[0-3],wave[0-6],pitch[0-127],envelope[0-4],length[0-127],mod[0-127:64=no mod]
          edgar.setupVoice(0, Voices[0][0], Voices[0][1], Voices[0][2], Voices[0][3], Voices[0][4]);
          edgar.setupVoice(1, Voices[1][0], Voices[1][1], Voices[1][2], Voices[1][3], Voices[1][4]);
          edgar.setupVoice(2, Voices[2][0], Voices[2][1], Voices[2][2], Voices[2][3], Voices[2][4]);
          edgar.setupVoice(3, Voices[3][0], Voices[3][1], Voices[3][2], Voices[3][3], Voices[3][4]);

        }
        break;
    }

  }

  if (micros() > nextentrytime)
  {
    nextentrytime = micros() + ((unsigned long)10000 * voicetime);

    edgar.trigger(voiceplay++);

    if (voiceplay > 3)
    {
      voiceplay = 0;
    }
  }

}





