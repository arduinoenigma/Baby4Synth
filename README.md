# Baby4Synth

Runs on an Arduino UNO, probably also works on a Nano, Micro and other 328p based boards

Uses this library:
https://github.com/dzlonline/the_synth

Connect an audio jack to pins 11 and 3. Use a 4.7K or similar resistor in one of the wires.

Send it a command like this:<br>
V0;W0;P50;E2;L90;M64;T25;<br>

V: voice 0..3<br>
W: wave  0..6<br>
P: pitch 0..127<br>
E: envelope 0..4<br>
L: length 0..127<br>
M: mod 0..127 64 no mod<br>
T: time interval in 1/100 of a second<br>